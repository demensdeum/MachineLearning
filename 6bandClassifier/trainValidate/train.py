import fasttext
import os

TRAIN_TIME = 10

os.chdir("trainValidate")
model = fasttext.train_supervised("train.txt", autotuneValidationFile="test.txt", autotuneDuration=TRAIN_TIME)
model.save_model("model.bin")
