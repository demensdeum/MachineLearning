import numpy as np
import sys
import random
import fasttext

LINES_COUNT_LIMIT = 67

clazzes = sys.argv[1].split(",")
ratio = float(sys.argv[2])

trainFile = open("trainValidate/train.txt", "w")
testFile = open("trainValidate/test.txt", "w")

def distributeCut(dataList):
    global LINES_COUNT_LIMIT
    if len(dataList) < LINES_COUNT_LIMIT:
        return []

    return dataList[:LINES_COUNT_LIMIT]

def distributeShuffle(dataList):
    copy = dataList.copy()
    random.shuffle(copy)
    return copy

input(f"Classes count: {len(clazzes)} [Press Enter!!]")

toLowerEnabled = True
isShuffleEnabled = True
distributionMethod = distributeCut

for clazzItem in enumerate(clazzes):
    clazz = clazzItem[1]
    print(clazz)
    clazzFilename = f"data/classes/{clazz}.txt"
    clazzFile = open(clazzFilename, "r")
    clazzList = list()
    for line in clazzFile:
        clazzLine = f"__label__{clazz} {line}"
        if toLowerEnabled:
            clazzLine = clazzLine.lower()
        clazzList.append(clazzLine)
    clazzFile.close()

    clazzList = distributeCut(clazzList)
    if isShuffleEnabled:
        clazzList = distributeShuffle(clazzList)
    if len(clazzList) < LINES_COUNT_LIMIT:
        continue
    print(clazzItem[0])
    print(len(clazzList))
    lenRatio = int(len(clazzList) * ratio)
    lhs = clazzList[:lenRatio]
    rhs = clazzList[lenRatio:]

    for line in lhs:
        trainFile.write(line)

    for line in rhs:
        testFile.write(line)

trainFile.close()
testFile.close()
