import fasttext
import os

os.chdir("trainValidate")
model = fasttext.load_model("model.bin")

failCasesFile = open("failCases.txt", "w")
testFile = open("test.txt", "r")

allLinesCount = 0
failCasesCount = 0

for line in testFile:
    allLinesCount += 1
    splitted = line.split(" ")
    clazz = splitted.pop(0)
    joined = " ".join(splitted).rstrip()
    predictResult = model.predict(joined)
    predictedClazz = predictResult[0][0]
    if clazz != predictedClazz:
        clazzFormatted = clazz.replace("__label__", "")
        predictedClazzFormatted = predictedClazz.replace("__label__", "")
        failCasesFile.write(f"Not \"{clazzFormatted}\" but \"{predictedClazzFormatted}\" {joined}\n")
        failCasesCount += 1
testFile.close()
failCasesFile.close()

failRatio = (failCasesCount / allLinesCount) * 100
print(f"Fail ratio: {failRatio}% ({failCasesCount}/{allLinesCount})")
