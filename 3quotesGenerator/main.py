from textgenrnn import textgenrnn
from os import path
import sys
from random import uniform
import string
import hunspell

weights_filename = "textgenrnn_weights.hdf5"
vocab_filename = "textgenrnn_vocab.json"
config_filename = "textgenrnn_config.json"
default_dataset_filename = "dataset_quotes_ru.txt"

already_generated = []

print("Textgenrnn quotes text generator by demensdeum 2018 (demensdeum@gmail.com)")

spellChecker = hunspell.HunSpell("index.dic", "index.aff")

command_mode = False

if len(sys.argv) == 2:
	print("Silent mode enabled")
	command_mode = True
	command_mode_state = sys.argv[1]
	command_mode_state_array = command_mode_state.split(",")
	
if command_mode == False:
	state = input("train_reset/train_resume/generate/generate_unique? ")
else:
	state = command_mode_state_array[0]

if state != "train_reset" and state != "train_resume" and state != "generate" and state != "generate_unique":
    print("Write train_reset or train_resume or generate or generate_unique... Exit")
    exit(1)

if state == "train_reset":
	textgen = textgenrnn()
    
elif state == "train_resume":
	if path.exists(weights_filename) ==  False:
		print("There is no weights to generate, train first... Exit")
		exit(2)
    	
	textgen = textgenrnn(weights_path = weights_filename, vocab_path = vocab_filename, config_path = config_filename)
          
if command_mode == False:
	if state == "train_reset" or state == "train_resume":
		dataset_file = input("dataset filename? (%s) " % default_dataset_filename)
		if len(dataset_file) < 1:
			dataset_file = default_dataset_filename
	elif state == "generate" or state == "generate_unique":
		tries = input("tries? (1) ")
		if len(tries) > 0:
			try:
				tries = int(tries)
			except:
				print("Incorrect generate phrase tries number - must be integer number, for example - 10")
		else:
			tries = 1
    	
elif command_mode == True:
	dataset_file = command_mode_state_array[1]
    
if state == "train_reset" or state == "train_resume":
	reset_model = state == "train_reset"
	while True:
		print("Endless train mode, every 4 epochs will be saved. CTRL+C to exit")
		try:
			textgen.train_from_file(dataset_file, num_epochs = 4, batch_size = 10024, new_model = reset_model)
		except KeyboardInterrupt:
			print("\nKilled")
			exit(0)
		except:
			print("Crashed... Recovery")
    
elif state == "generate" or state == "generate_unique":

	if path.exists(weights_filename) ==  False:
		print("There is no weights to generate, train first... Exit")
		exit(3)

	if command_mode == True:
		tries = command_mode_state_array[1]
		try:
			tries = int(tries)
		except:
			print("Incorrect generate phrase tries number - must be integer number, for example - generate,10")
		
	if state == "generate_unique":	
		if command_mode == True:
			if len(command_mode_state_array) != 3:
				print("Incorrect generate_unique format, must be - generate_unique,10,dataset.txt")
				exit(5)
			else:
				dataset_file = command_mode_state_array[2]
		else:
			dataset_file = input("dataset filename? (%s) " % default_dataset_filename)
			if len(dataset_file) < 1:
				dataset_file = default_dataset_filename
				
		dataset_file_descriptor = open(dataset_file,"r")
		dataset_file_text = dataset_file_descriptor.read()
		dataset_file_text_set = set(dataset_file_text.split("\n"))
		
		filtered_dataset_file_text_set = []
		
		for line in dataset_file_text_set:
				line = ''.join(character for character in line if character not in string.punctuation)
				filtered_dataset_file_text_set.append(line)
		
		if len(dataset_file_text_set) < 1:
			print("Incorrent dataset... Exit")
			exit(6)

	for tryStep in range(0, tries):
		textgen = textgenrnn(weights_path = weights_filename, vocab_path = vocab_filename, config_path = config_filename)
		
		if state == "generate":
			generated = textgen.generate(temperature = uniform(0.5, 0.6), return_as_list=True)
			print(generated[0])
			
		elif state == "generate_unique":
			generated = list(dataset_file_text_set)[0]
			while generated in filtered_dataset_file_text_set or generated in already_generated:
				generated = textgen.generate(temperature = uniform(0.2, 2.0), return_as_list=True)
				generated = generated[0]
				generated = ''.join(character for character in generated if character not in string.punctuation)
				generated = generated.split()
				for word in generated:
					if spellChecker.spell(word) == False:
						generated = list(dataset_file_text_set)[0]
						break						
				generated = ' '.join(generated)
			print(generated)
			already_generated.append(generated)
			
exit(0)