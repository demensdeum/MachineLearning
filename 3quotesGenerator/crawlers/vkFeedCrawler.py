import re
import sys

regexp = "wall_post_text\">(.*?)\</div"

filename = sys.argv[1]
file = open(filename, "r")

entireFile = file.read()
file.close()

match = re.findall(regexp, entireFile)

formattedMatches = []

for m in match:
	m = m.replace("<br>"," ")
	if m.startswith("<a href"):
		continue
	m = m.split("<br> <br>")[0].split("    ")[0]
	formattedMatches.append(m)
	print(m)

file = open("output.txt","w")
file.write("\n".join(formattedMatches))
file.close()